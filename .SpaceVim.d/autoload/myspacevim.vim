function! myspacevim#before() abort
    set wrap
    " set clipboard+=unnamedplus
endfunction

function! myspacevim#after() abort
    iunmap jk
endfunction
