# pylint: disable=C0111
from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer  # noqa: F401
config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

import draw

config.bind("<shift-j>", "tab-prev")
config.bind("<shift-k>", "tab-next")
config.bind("<shift-escape>", "enter-mode passthrough")
config.bind(">", "tab-move +")
config.bind("<", "tab-move -")
config.bind("T", "tab-focus last")
config.bind(",ba", "set-cmd-text :open -t sv.bab.la/lexikon/engelsk-svensk/")
config.bind(",sv", "set-cmd-text :open -t synonymer.se/sv-syn/")
config.bind(",th", "set-cmd-text :open -t thesaurus.com/browse/")
config.bind(",yt", "set-cmd-text :open -t youtube.com/results?search_query=")

config.unbind("<ctrl+v>")

c.confirm_quit = ["multiple-tabs"]
c.session.lazy_restore = True
c.auto_save.session = True
c.content.pdfjs = True
c.tabs.pinned.shrink = False
c.content.notifications = True

draw.blood(c, {'spacing': {'vertical': 2, 'horizontal': 8}})
